-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 09 2019 г., 19:32
-- Версия сервера: 10.3.13-MariaDB
-- Версия PHP: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: ` ticketbooking`
--

-- --------------------------------------------------------

--
-- Структура таблицы `age_rating`
--

CREATE TABLE `age_rating` (
  `id` int(11) NOT NULL,
  `short_name` varchar(6) NOT NULL,
  `long_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `age_rating`
--

INSERT INTO `age_rating` (`id`, `short_name`, `long_name`) VALUES
(1, 'G', 'без ограничений'),
(2, 'M', 'все, но дети в сопр. взр.'),
(3, 'R', '16- в сопр. взросл'),
(4, 'X', 'если 17- то нельзя');

-- --------------------------------------------------------

--
-- Структура таблицы `film`
--

CREATE TABLE `film` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `duration` int(11) NOT NULL,
  `premiere_date` date NOT NULL,
  `id_age_rating` int(11) NOT NULL,
  `id_main_genre` int(11) NOT NULL,
  `description` text NOT NULL,
  `logo_link` varchar(255) NOT NULL,
  `trailer_link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `film`
--

INSERT INTO `film` (`id`, `name`, `duration`, `premiere_date`, `id_age_rating`, `id_main_genre`, `description`, `logo_link`, `trailer_link`) VALUES
(1, 'Джокер', 121, '2019-10-03', 3, 2, 'Готэм, начало 1980-х годов. Комик Артур Флек живет с больной матерью, которая с детства учит его «ходить с улыбкой». Пытаясь нести в мир хорошее и дарить людям радость, Артур сталкивается с человеческой жестокостью и постепенно приходит к выводу, что этот мир получит от него не добрую улыбку, а ухмылку злодея Джокера.', 'https://st.kp.yandex.net/images/film_iphone/iphone360_1048334.jpg', 'https://youtu.be/50IJyz7ecqc'),
(2, 'Оно', 135, '2017-09-07', 3, 3, 'Небольшой городок в штате Мэн терроризирует загадочный серийный убийца, с нечеловеческой жестокостью умертвляющий детей. Тут и там находят разорванные тела, а иногда только их части. Семеро одиннадцатилетних ребят: Ричи Тозиер, Билл Денбро, Беверли Марш, Майк Хэнлон, Эдди Каспбрак, Бен Хэнском и Стэн Урис сталкиваются каждый по отдельности с загадочным злом — ужасающим монстром, способным принимать любые формы. В фильме Оно предстает в образе клоуна под названием Пеннивайз, заманивающего детей оранжевыми помпонами и воздушными шариками. В месте, известном как «Степи», ребята, объединившись в «Клуб Неудачников», решают найти и уничтожить чудовище. Лидером «Неудачников» становится Билл Денбро, брат которого был убит Пеннивайзом за год до этого, и теперь у Билла своя вендетта. Кроме монстра «Неудачников» преследует местный хулиган Генри Бауэрс со своими дружками, что, впрочем, ещё сильнее сплачивает друзей.', 'https://www.film.ru/sites/default/files/styles/thumb_260x400/public/movies/posters/16607995-946826.jpg', 'https://youtu.be/IisU-JHj_fU');

-- --------------------------------------------------------

--
-- Структура таблицы `genre`
--

CREATE TABLE `genre` (
  `id` int(11) NOT NULL,
  `short_name` varchar(6) NOT NULL,
  `long_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `genre`
--

INSERT INTO `genre` (`id`, `short_name`, `long_name`) VALUES
(1, 'ком', 'комедия'),
(2, 'пф', 'приключенческий фильм'),
(3, 'уж', 'ужас'),
(4, 'дет', 'детектив');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `age_rating`
--
ALTER TABLE `age_rating`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_age_rating` (`id_age_rating`),
  ADD KEY `id_main_genre` (`id_main_genre`);

--
-- Индексы таблицы `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `age_rating`
--
ALTER TABLE `age_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `film`
--
ALTER TABLE `film`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `genre`
--
ALTER TABLE `genre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `film`
--
ALTER TABLE `film`
  ADD CONSTRAINT `film_ibfk_1` FOREIGN KEY (`id_age_rating`) REFERENCES `age_rating` (`id`),
  ADD CONSTRAINT `film_ibfk_2` FOREIGN KEY (`id_main_genre`) REFERENCES `genre` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
