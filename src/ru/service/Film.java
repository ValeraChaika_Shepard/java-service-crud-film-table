package ru.service;

/**
 * Created by valer on 09.11.2019.
 */
public class Film implements Entry{
    private String id;
    private String name;
    private String duration;
    private String premiere_date;
    private String id_age_rating;
    private String id_main_genre;
    private String description;
    private String logo_link;
    private String trailer_link;


    public Film() {}

    public Film(String id, String name, String duration, String id_age_rating, String id_main_genre, String description) {
        this.id = id;
        this.name = name;
        this.duration = duration;
        this.id_age_rating = id_age_rating;
        this.id_main_genre = id_main_genre;
        this.description = description;
    }

    public Film(String name, String duration, String premiere_date, String id_age_rating, String id_main_genre, String description, String logo_link, String trailer_link) {
        this.name = name;
        this.duration = duration;
        this.premiere_date = premiere_date;
        this.id_age_rating = id_age_rating;
        this.id_main_genre = id_main_genre;
        this.description = description;
        this.logo_link = logo_link;
        this.trailer_link = trailer_link;
    }

    public Film(String id, String name, String duration, String premiere_date, String id_age_rating, String id_main_genre, String description, String logo_link, String trailer_link) {
        this.id = id;
        this.name = name;
        this.duration = duration;
        this.premiere_date = premiere_date;
        this.id_age_rating = id_age_rating;
        this.id_main_genre = id_main_genre;
        this.description = description;
        this.logo_link = logo_link;
        this.trailer_link = trailer_link;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPremiere_date() {return premiere_date;}

    public void setPremiere_date(String premiere_date) {
        this.premiere_date = premiere_date;
    }

    public String getId_age_rating() {
        return id_age_rating;
    }

    public void setId_age_rating(String id_age_rating) {
        this.id_age_rating = id_age_rating;
    }

    public String getId_main_genre() {
        return id_main_genre;
    }

    public void setId_main_genre(String id_main_genre) {
        this.id_main_genre = id_main_genre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogo_link() {
        return logo_link;
    }

    public void setLogo_link(String logo_link) {
        this.logo_link = logo_link;
    }

    public String getTrailer_link() {
        return trailer_link;
    }

    public void setTrailer_link(String trailer_link) {
        this.trailer_link = trailer_link;
    }

    @Override
    public String toString() {
        return "Film{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", duration='" + duration + '\'' +
                ", premiere_date='" + premiere_date + '\'' +
                ", id_age_rating='" + id_age_rating + '\'' +
                ", id_main_genre='" + id_main_genre + '\'' +
                ", description='" + description + '\'' +
                ", logo_link='" + logo_link + '\'' +
                ", trailer_link='" + trailer_link + '\'' +
                '}';
    }
}
