package ru.service.server;

// это аннотации, т.е. способ отметить наши классы и методы,
// как связанные с веб-сервисной технологией
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * Created by valer on 09.11.2019.
 */

// говорим, что наш интерфейс будет работать как веб-сервис
@WebService
// говорим, что веб-сервис будет использоваться для вызова методов
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface Lab3JavaWebService {
    // говорим, что этот метод можно вызывать удаленно
    @WebMethod
    public String getFilmByID(String id);

    @WebMethod
    public String getAllFilms();

    @WebMethod
    public Boolean createFilms(String jsData);

    @WebMethod
    public Boolean deleteFilm(String id);

    @WebMethod
    public Boolean editFilm(String id, String jsData);
}
