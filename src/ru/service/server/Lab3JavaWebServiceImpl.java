package ru.service.server;

// таже аннотация, что и при описании интерфейса,
import javax.jws.WebService;

import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.ArrayList;

import com.sun.org.apache.xerces.internal.xs.datatypes.ObjectList;
import com.sun.org.apache.xpath.internal.SourceTree;
import org.json.*;
import ru.service.Film;

/**
 * Created by valer on 09.11.2019.
 */
// но здесь используется с параметром endpointInterface,
// указывающим полное имя класса интерфейса нашего веб-сервиса

@WebService(endpointInterface = "ru.service.server.Lab3JavaWebService")
public class Lab3JavaWebServiceImpl implements Lab3JavaWebService{

    private static final String url = "jdbc:mysql://127.0.0.1:3306/ ticketbooking";
    private static final String user = "root";
    private static final String password = "";

    private static Connection con;
    private static Statement stmt;
    private static ResultSet rs;

    @Override
    public String getFilmByID(String id) {
        Film tmp = null;

        try {
            if (con == null || con.isClosed()) {
                try {
                    Class.forName("com.mysql.jdbc.Driver");
                } catch (ClassNotFoundException e) {
                    System.out.println(e.getMessage());
                }

                con = DriverManager.getConnection(url, user, password);
            }

            stmt = con.createStatement();

            int idInt = Integer.parseInt(id);
            System.out.println("YCТАЛ СИЛЬНО ОЧЕНЬ : " + idInt);

            rs = stmt.executeQuery(String.format("SELECT * FROM film Where id='%d';", idInt));
            while(rs.next()){
                tmp = (new Film(rs.getString("id"),
                        rs.getString("name"),
                        rs.getString("duration"),
                        rs.getString("premiere_date"),
                        rs.getString("id_age_rating"),
                        rs.getString("id_main_genre"),
                        rs.getString("description"),
                        rs.getString("logo_link"),
                        rs.getString("trailer_link")));
            }

            //System.out.println("ПОЛУЧИЛИ : " + tmp.toString());

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            //close connection ,stmt and resultset here
            try { con.close(); } catch(SQLException se) { /*can't do anything */ }
            try { stmt.close(); } catch(SQLException se) { /*can't do anything */ }
            try { rs.close(); } catch(SQLException se) { /*can't do anything */ }
        }

        JSONObject jsObj = new JSONObject(tmp);
        //System.out.println("С Сервиса вернем : " + jsObj.toString());

        return jsObj.toString();

    }


//    Метод, который возвращает все фильмы из БД
    @Override
    public String getAllFilms() {
        ArrayList<Film> resList = new ArrayList<Film>();
        ArrayList<Film> filmsFromJSA = new ArrayList<Film>();

        try {
            if (con == null || con.isClosed()) {
                try {
                    Class.forName("com.mysql.jdbc.Driver");
                } catch (ClassNotFoundException e) {
                    System.out.println(e.getMessage());
                }

                con = DriverManager.getConnection(url, user, password);
            }

            stmt = con.createStatement();

            rs = stmt.executeQuery("SELECT * FROM film");
            while(rs.next()){
                resList.add(new Film(rs.getString("id"),
                        rs.getString("name"),
                        rs.getString("duration"),
                        rs.getString("premiere_date"),
                        rs.getString("id_age_rating"),
                        rs.getString("id_main_genre"),
                        rs.getString("description"),
                        rs.getString("logo_link"),
                        rs.getString("trailer_link")));
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            //close connection ,stmt and resultset here
            try { con.close(); } catch(SQLException se) { /*can't do anything */ }
            try { stmt.close(); } catch(SQLException se) { /*can't do anything */ }
            try { rs.close(); } catch(SQLException se) { /*can't do anything */ }
        }

        JSONArray jsArray = new JSONArray(resList);

        return jsArray.toString();
    }

    @Override
    public Boolean createFilms(String str) {
        try {
            if (con == null || con.isClosed()) {
                try {
                    Class.forName("com.mysql.jdbc.Driver");
                } catch (ClassNotFoundException e) {
                    System.out.println(e.getMessage());
                }

                con = DriverManager.getConnection(url, user, password);
            }

            JSONObject jso = new JSONObject(str);

            String name = jso.get("name").toString();
            String duration = jso.get("duration").toString();
            String premiere_date = jso.get("premiere_date").toString();
            String id_age_rating = jso.get("id_age_rating").toString();
            String id_main_genre = jso.get("id_main_genre").toString();
            String description = jso.get("description").toString();
            String logo_link = jso.get("logo_link").toString();
            String trailer_link = jso.get("trailer_link").toString();

            System.out.println(name + duration + premiere_date + id_age_rating + id_main_genre + description + logo_link + trailer_link);

            Statement ps = con.createStatement();
            ps.execute(String.format("INSERT INTO film (name, duration, premiere_date, id_age_rating, id_main_genre, description, logo_link, trailer_link)" +
                            " VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');",
                    name,
                    duration,
                    premiere_date,
                    id_age_rating,
                    id_main_genre,
                    description,
                    logo_link,
                    trailer_link));
            ps.close();

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            //close connection ,stmt and resultset here
            try { con.close(); } catch(SQLException se) { /*can't do anything */ }
            try { stmt.close(); } catch(SQLException se) { /*can't do anything */ }
            try { rs.close(); } catch(SQLException se) { /*can't do anything */ }
        }
        return true;
    }


    @Override
    public Boolean deleteFilm(String id) {
        try {
            if (con == null || con.isClosed()) {
                try {
                    Class.forName("com.mysql.jdbc.Driver");
                } catch (ClassNotFoundException e) {
                    System.out.println(e.getMessage());
                }

                con = DriverManager.getConnection(url, user, password);
            }

            Statement ps = con.createStatement();
            ps.execute("DELETE FROM film WHERE id=" + id);
            ps.close();

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            //close connection ,stmt and resultset here
            try { con.close(); } catch(SQLException se) { /*can't do anything */ }
            try { stmt.close(); } catch(SQLException se) { /*can't do anything */ }
            try { rs.close(); } catch(SQLException se) { /*can't do anything */ }
        }
        return true;
    }

    @Override
    public Boolean editFilm(String id, String jsData) {
        try {
            if (con == null || con.isClosed()) {
                try {
                    Class.forName("com.mysql.jdbc.Driver");
                } catch (ClassNotFoundException e) {
                    System.out.println(e.getMessage());
                }

                con = DriverManager.getConnection(url, user, password);
            }

            JSONObject jso = new JSONObject(jsData);

            String name = jso.get("name").toString();
            String duration = jso.get("duration").toString();
            String premiere_date = jso.get("premiere_date").toString();
            String id_age_rating = jso.get("id_age_rating").toString();
            String id_main_genre = jso.get("id_main_genre").toString();
            String description = jso.get("description").toString();
            String logo_link = jso.get("logo_link").toString();
            String trailer_link = jso.get("trailer_link").toString();

            Statement ps = con.createStatement();

            int idInt = Integer.parseInt(id);
            ps.executeUpdate(String.format("UPDATE film SET name = '%s', duration = '%s', premiere_date = '%s', id_age_rating = '%s', " +
                            "id_main_genre = '%s', description = '%s', logo_link = '%s', trailer_link = '%s' " +
                            "WHERE id = '%d';",
                    name, duration, premiere_date, id_age_rating,
                    id_main_genre, description, logo_link, trailer_link,
                    idInt));
            ps.close();

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            //close connection ,stmt and resultset here
            try { con.close(); } catch(SQLException se) { /*can't do anything */ }
            try { stmt.close(); } catch(SQLException se) { /*can't do anything */ }
            try { rs.close(); } catch(SQLException se) { /*can't do anything */ }
        }
        return true;
    }
}
