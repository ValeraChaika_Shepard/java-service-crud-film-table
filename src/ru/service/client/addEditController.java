package ru.service.client;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.json.JSONObject;
import ru.service.Film;


import javafx.stage.Stage;

import javafx.scene.control.TextField;
import ru.service.server.Lab3JavaWebService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by valer on 09.11.2019.
 */
public class addEditController {
    private QName qname = new QName("http://server.service.ru/", "Lab3JavaWebServiceImplService");


    private Controller main;
    private Stage stage;

    private boolean status;
    private String filmIdToEdit;

    @FXML TextField et1;
    @FXML TextField et2;
    @FXML TextField et3;
    @FXML TextField et4;
    @FXML TextField et5;
    @FXML TextField et6;
    @FXML TextField et7;

    @FXML TextArea area1;


    public void setMain(Controller main) {this.main = main;}
    public void setDialogStage(Stage stage) {this.stage = stage;}
    public void setStatus(boolean status) {this.status = status;}
    public void setFilmIdToEdit(String filmIdToEdit) {this.filmIdToEdit = filmIdToEdit;}


    @FXML
    public void okHandler() {
        //Тут можно кучу проверок понаделать, но мне лень!
        String name = et1.getText();
        String duration = et2.getText();
        String date_prem = et3.getText();
        String id_age_ogr = et4.getText();
        String id_osn_ganra = et5.getText();
        String logo_url = et6.getText();
        String trailer_url = et7.getText();
        String descript = area1.getText();

        Film newFilm = new Film(name, duration, date_prem, id_age_ogr, id_osn_ganra, descript, logo_url, trailer_url);
        JSONObject jsO = new JSONObject(newFilm);

        if(status == false) {
            try {
            /////Передаем jsO.toString() в параметр к запросу на сервис////
            URL url = new URL("http://localhost:1986/wss/createFilm?wsdl");
            Service service = Service.create(url, qname);
            Lab3JavaWebService ws = service.getPort(Lab3JavaWebService.class);

            Boolean res = ws.createFilms(jsO.toString());
            if(res) {
                stage.close();
                main.fillDataToTableView();
                main.showAlert("Новый фильм добавлен", "Успешно");
            } else {
                stage.close();
                main.showAlert("Новый фильм не добавлен", "Ошибка");
            }

            } catch (MalformedURLException e) {
                main.showAlert("Сервер выключен!", "Ошибка");
                e.printStackTrace();
            }
        } else {
            try {
                URL url = new URL("http://localhost:1986/wss/createFilm?wsdl");
                Service service = Service.create(url, qname);
                Lab3JavaWebService ws = service.getPort(Lab3JavaWebService.class);

                Boolean res = ws.editFilm(filmIdToEdit, jsO.toString());
                if(res) {
                    stage.close();
                    main.fillDataToTableView();
                    main.showAlert("Фильм изменен", "Успешно");
                } else {
                    stage.close();
                    main.showAlert("Фильм не изменен", "Ошибка");
                }

            } catch (MalformedURLException e) {
                main.showAlert("Сервер выключен!", "Ошибка");
                e.printStackTrace();
            }

            System.out.println("Сейчас будем пытаться изменять объект OK");
        }

    }

    @FXML
    public void cancelHandler() {
        stage.close();
    }


    public void displayFilmToEdit() {
        URL url = null;
        try {
            url = new URL("http://localhost:1986/wss/getFilmByID?wsdl");
            Service service = Service.create(url, qname);
            Lab3JavaWebService ws = service.getPort(Lab3JavaWebService.class);

            String jsObjFilm = ws.getFilmByID(filmIdToEdit);
            JSONObject jso = new JSONObject(jsObjFilm);

            et1.setText(jso.get("name").toString());
            et2.setText(jso.get("duration").toString());
            et3.setText(jso.get("premiere_date").toString());
            et4.setText(jso.get("id_age_rating").toString());
            et5.setText(jso.get("id_main_genre").toString());
            et6.setText(jso.get("logo_link").toString());
            et7.setText(jso.get("trailer_link").toString());

            area1.setText(jso.get("description").toString());

        } catch (Exception e) {
            main.showAlert("Сервер выключен!", "Ошибка");
            e.printStackTrace();
        }
    }
}
