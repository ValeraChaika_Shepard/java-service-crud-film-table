package ru.service.client;

import javafx.fxml.FXML;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ru.service.Entry;
import ru.service.Film;
import ru.service.server.Lab3JavaWebService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;


import com.sun.org.apache.xpath.internal.operations.Or;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;


import javax.swing.plaf.LabelUI;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;


public class Controller {
    // Параметры следующего конструктора смотрим в самом первом теге WSDL описания - definitions
    // 1-ый аргумент смотрим в атрибуте targetNamespace
    // 2-ой аргумент смотрим в атрибуте name
    private QName qname = new QName("http://server.service.ru/", "Lab3JavaWebServiceImplService");

    ObservableList<Entry> data;
    @FXML TableView<Entry> tableView;

    @FXML TableColumn<Entry,String> tc1;
    @FXML TableColumn<Entry,String> tc2;
    @FXML TableColumn<Entry,String> tc3;
    @FXML TableColumn<Entry,String> tc4;
    @FXML TableColumn<Entry,String> tc5;
    @FXML TableColumn<Entry,String> tc6;

    private ArrayList<Film> filmsFromJSA = null;

    //SetStatus(false) - на добавление, объект не передаем
    //SetStatus(true) - на изменение, нужно еще объект передать SetFilm

    public void fillDataToTableView() throws MalformedURLException {
        // создаем ссылку на wsdl описание
        URL url = new URL("http://localhost:1986/wss/getAllFilms?wsdl");
        // Теперь мы можем дотянуться до тега service в wsdl описании,
        Service service = Service.create(url, qname);
        // а далее и до вложенного в него тега port, чтобы
        // получить ссылку на удаленный от нас объект веб-сервиса
        Lab3JavaWebService ws = service.getPort(Lab3JavaWebService.class);
        // Ура! Теперь можно вызывать удаленный метод


        String jsArrDataString = ws.getAllFilms();

        JSONArray jsonArray = null;

        try {
            jsonArray = new JSONArray(jsArrDataString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println(jsonArray);

        if(filmsFromJSA != null) {
            filmsFromJSA.clear();
            filmsFromJSA = null;
        }

        filmsFromJSA = new ArrayList<Film>();

        data.clear();

        for(int i = 0; i < jsonArray.length(); i++) {
            Film tmp = new Film();
            try {
                JSONObject jso = jsonArray.getJSONObject(i);

                tmp.setId(jso.get("id").toString());
                tmp.setName(jso.get("name").toString());
                tmp.setDuration(jso.get("duration").toString());
                tmp.setPremiere_date(jso.get("premiere_date").toString());
                tmp.setId_age_rating (jso.get("id_age_rating").toString());
                tmp.setId_main_genre (jso.get("id_main_genre").toString());
                tmp.setDescription(jso.get("description").toString());
                tmp.setLogo_link(jso.get("logo_link").toString());
                tmp.setTrailer_link(jso.get("trailer_link").toString());

                //id,name,duration,возр_рейт,осн_жанр,описание
                data.add(new Film(tmp.getId(), tmp.getName(), tmp.getDuration(), tmp.getId_age_rating(), tmp.getId_main_genre(), tmp.getDescription()));

                filmsFromJSA.add(tmp);
            } catch (JSONException e) {
                showAlert("Сервер выключен!", "Ошибка");
                e.printStackTrace();
            }
            System.out.println(tmp);
        }
    }

    @FXML private void initialize() {
        data = tableView.getItems();

        tc1.setCellValueFactory(new PropertyValueFactory<Entry,String>("id"));
        tc2.setCellValueFactory(new PropertyValueFactory<Entry,String>("name"));
        tc3.setCellValueFactory(new PropertyValueFactory<Entry,String>("duration"));
        tc4.setCellValueFactory(new PropertyValueFactory<Entry,String>("id_age_rating"));
        tc5.setCellValueFactory(new PropertyValueFactory<Entry,String>("id_main_genre"));
        tc6.setCellValueFactory(new PropertyValueFactory<Entry,String>("description"));

        try {
            fillDataToTableView();
        } catch (Exception e) {
            showAlert("Сервер выключен!", "Ошибка");
            e.printStackTrace();
        }
    }

    @FXML
    public void addFilm() throws MalformedURLException {
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Controller.class.getResource("addLayout.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Добавления фильма в БД");

            dialogStage.initModality(Modality.WINDOW_MODAL);
            //dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            addEditController controller = loader.getController();
            controller.setMain(this);
            controller.setDialogStage(dialogStage);

            controller.setStatus(false);

            dialogStage.showAndWait();
        } catch (Exception e) {
            showAlert("Сервер выключен!", "Ошибка");
            e.printStackTrace();
        }
    }

    @FXML
    public void deleteFilm() throws MalformedURLException {
        String id = "-1";
        int indexInTable = tableView.getSelectionModel().getSelectedIndex();

        if(indexInTable == -1) {
            showAlert("Нужно выбрать Фильм, чтобы его удалить", "Внимание");
        } else {
            id = ((Film)tableView.getItems().get(indexInTable)).getId();
            // создаем ссылку на wsdl описание
            URL url = new URL("http://localhost:1986/wss/deleteFilm?wsdl");
            // Теперь мы можем дотянуться до тега service в wsdl описании,
            Service service = Service.create(url, qname);
            // а далее и до вложенного в него тега port, чтобы
            // получить ссылку на удаленный от нас объект веб-сервиса
            Lab3JavaWebService ws = service.getPort(Lab3JavaWebService.class);

            Boolean res = ws.deleteFilm(id);
            if(res) {
                fillDataToTableView();
                showAlert("Фмльм удален", "Успешно");
            } else {
                showAlert("Фильм не удален", "Ошибка");
            }
        }
    }

    @FXML
    public void editFilm() throws MalformedURLException {
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Controller.class.getResource("addLayout.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Изменения фильма");

            dialogStage.initModality(Modality.WINDOW_MODAL);
            //dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            addEditController controller = loader.getController();
            controller.setMain(this);
            controller.setDialogStage(dialogStage);

            controller.setStatus(true);
            int indexInTable = tableView.getSelectionModel().getSelectedIndex();
            if(indexInTable == -1) {
                showAlert("Нужно выбрать фильм, чтобы изменить его", "Внимание");
            } else {
                String filmIdToEdit = ((Film)tableView.getItems().get(indexInTable)).getId();

                //System.out.println("Я УСТАААААЛ :   " + filmIdToEdit);

                controller.setFilmIdToEdit(filmIdToEdit);
                controller.displayFilmToEdit();

                dialogStage.showAndWait();
            }
        } catch (Exception e) {
            showAlert("Сервер выключен!", "Ошибка");
            e.printStackTrace();
        }
    }

    public void showAlert(String text, String header)
    {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        //alert.initOwner(getPrimaryStage());
        alert.setTitle(header);
        alert.setHeaderText(header);
        alert.setContentText(text);
        alert.showAndWait();
    }
}
