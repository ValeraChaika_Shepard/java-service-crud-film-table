package ru.service.client;

// нужно, чтобы получить wsdl описание и через него
// дотянуться до самого веб-сервиса
import java.net.URL;
// такой эксепшн возникнет при работе с объектом URL
import java.net.MalformedURLException;

// классы, чтобы пропарсить xml-ку c wsdl описанием
// и дотянуться до тега service в нем
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

// интерфейс нашего веб-сервиса (нам больше и нужно)
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import ru.service.Entry;
import ru.service.Film;


import javafx.fxml.FXML;
import ru.service.Entry;
import ru.service.server.Lab3JavaWebService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

import com.sun.org.apache.xpath.internal.operations.Or;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;


import javax.swing.plaf.LabelUI;
import java.io.IOException;
import java.sql.SQLException;

import javafx.scene.control.TableColumn;


import ru.service.server.Lab3JavaWebService;

/**
 * Created by valer on 09.11.2019.
 */
public class Lab3JavaWebServiceClient extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Сервис: Клиентская сторона");
        primaryStage.setScene(new Scene(root, 900, 500));

        primaryStage.show();
    }

    public static void main(String[] args)  throws MalformedURLException {
        launch(args);
    }
}
