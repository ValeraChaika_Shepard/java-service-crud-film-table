package ru.service.endpoint;

// класс, для запуска веб-сервера с веб-сервисами
import javax.xml.ws.Endpoint;
// класс нашего веб-сервиса
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import ru.service.server.Lab3JavaWebServiceImpl;

import java.net.MalformedURLException;

/**
 * Created by valer on 09.11.2019.
 */
public class Lab3JavaWebServicePublisher extends Application {
    @FXML Label serverLabel;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("serv.fxml"));
        primaryStage.setTitle("Сервис: Серверная сторона");
        primaryStage.setScene(new Scene(root, 700, 200));
        primaryStage.show();
    }

    @FXML
    public void startServer()  {
        // запускаем веб-сервер на порту 1986
        // и по адресу, указанному в первом аргументе,
        // запускаем веб-сервис, передаваемый во втором аргументе
        Endpoint.publish("http://localhost:1986/wss/getFilmByID", new Lab3JavaWebServiceImpl());
        Endpoint.publish("http://localhost:1986/wss/getAllFilms", new Lab3JavaWebServiceImpl());
        Endpoint.publish("http://localhost:1986/wss/createFilm", new Lab3JavaWebServiceImpl());
        Endpoint.publish("http://localhost:1986/wss/deleteFilm", new Lab3JavaWebServiceImpl());
        Endpoint.publish("http://localhost:1986/wss/editFilm", new Lab3JavaWebServiceImpl());

        serverLabel.setText("Сервер запущен");
    }

    @FXML
    public void stopServer() {
        showAlert("Сервер остановлен", "Внимание");
        System.exit(0);
    }

    public void showAlert(String text, String header)
    {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(header);
        alert.setHeaderText(header);
        alert.setContentText(text);
        alert.showAndWait();
    }
}
